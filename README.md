### Cahier des charges du projet "intHairNet"

---

# 1 - CADRE DU PROJET #

### 1.1 - Résumé ###

see Rapport_DELORME_VERRIERE.docx

### 1.2 - Enjeux et objectifs ###

see Rapport_DELORME_VERRIERE.docx

# 2 - Run project #

## Run with
* npm   6.14.2
* node 13.10.1
* angular 9

### 2.1 - All in one with docker compose
```
cd inthairnet
docker-compose build
docker-compose run
```

REST API is available from
```
http://localhost:3000/
```

Swagger is available from
```
http://localhost:3000/api-docs
```

Front is available from 
```
http://localhost:4200/
```

### 2.2 - Launch the back end ###
```
cd inthairnet/back
npm start
```

or with docker:
```
cd inthairnet/back
docker build -t inthairnetback:dev .
docker run -it -p 3000:3000 inthairnetback:dev
```

Swagger is available from
```
http://localhost:3000/api-docs
```

### 2.2 - Launch the tests ###
```
cd inthairnet/back
npm test
```

### 2.3 - Launch the front end ###
```
cd inthairnet/front
ng serve
```

or with docker:
```
cd inthairnet/front
docker build -t inthairnetfront:dev .
docker run -it -p 4200:4200 inthairnetfront:dev
```

Front is available from 
```
http://localhost:4200/
```

# 3 - Spécifications techniques #

### 3.1 - Choix des technologies ###

* models has been generated with auto-sequelize:
```js
sequelize-auto -h [host] -d postgres -u [user] -x [pwd] -p [port] -e postgres -o ./models -C
```
* Token

Nous avons choisis d'utiliser JWT token pour gérer l'authentification et resteindre les routes.

### How to use ?

* POSTMAN : 

POST http://localhost:3000/api/v1/authenticate
Menu -> body (json) => saisir
{
  "mail": "titi@gmail.com",
  "pwd": "titi"
}
RUN la requete nous retourne un token

GET http://localhost:3000/api/v1/auth
Menu -> Authorization -> Type = Bearer Token => Saisir le token obtenu lors de l'authentification puis lancer la requete

* SWAGGER UI :

POST ​/authenticate
saisir 
{
  "mail": "titi@gmail.com",
  "pwd": "titi"
}
RUN la requete nous retourne un token

Dans le header de la page, bouton "Authorize" => saisir "Bearer <TOKEN_VALUE>"

GET ​/auth
Retourne les résultats.

Problèmes rencontrés : faire fonctionner les tokens sur swagger (paramétrage + saisie du mot clé Bearer !)


Tutoriel suivi :
* https://jasonwatmore.com/post/2018/08/06/nodejs-jwt-authentication-tutorial-with-example-api
* https://jasonwatmore.com/post/2018/05/23/angular-6-jwt-authentication-example-tutorial
* https://github.com/miguelduarte42/swagger-jwt-example/blob/master/api/swagger/swagger.yaml
* https://www.edureka.co/blog/docker-compose-containerizing-mean-stack-application/
* et les cours !

---

## TODO:

* Thibaud et Romain  : partie client
* Thibault et Dorian (nous) : partie coiffeur

---

## DONE:

* Authentification coiffeur

* Creation Salon
* Gestion Offre (offre = prestation + date = (duree + prix + coupe) + date)

* Gérer un planning 
	* Disponibilité 
	* Rdv (client + coiffeur + crénaux)

* Création d'un salon de coiffure
	* Coiffeur
	* Localisation
	* Horaires d'ouvertures
	* Informations pratiques

* Recherche d'offre avec autocomplete

* Création de coiffeur (personne réelle)
	* Expertises, spécialités (Homme / femme / barbe, etc ..)
	* Offre (Prix, préstation)
	* Présentation
	* Horaires
	* Manager de salon
	* Pagination calendrier (mois/semaine/jour)
	* Recherche par client calendrier
	* un beau Front

* Tout mettre dans un Docker!
* Utiliser dockercompose pour manage les deux docker comme un projet unique





