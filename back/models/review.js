/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('review', {
    idPrestation: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id_prestation'
    },
    idHairdresser: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id_hairdresser'
    },
    idClient: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id_client'
    },
    grade: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'grade'
    }
  }, {
    tableName: 'review'
  });
};
