/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('authentication', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      field: 'id'
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'password'
    }
  }, {
    tableName: 'authentication'
  });
};
