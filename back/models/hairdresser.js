/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hairdresser', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      field: 'id'
    },
    idAuthentication: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id_authentication'
    },
    idShop: {
      type: DataTypes.INTEGER,
      allowNull: true,
      primaryKey: true,
      field: 'id_shop'
    },
    isManager: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false,
      field: 'is_manager'
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'lastname'
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'firstname'
    },
    specialities: {
      type: DataTypes.JSON,
      allowNull: false,
      field: 'specialities'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'email'
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'phone_number'
    },
    disponibilities: {
      type: DataTypes.JSON,
      allowNull: true,
      field: 'disponibilities'
    },
    leaves: {
      type: DataTypes.JSON,
      allowNull: true,
      field: 'leaves'
    }
  }, {
    tableName: 'hairdresser'
  });
};
