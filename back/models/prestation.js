/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('prestation', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      field: 'id'
    },
    idHairdresser: {
      type: DataTypes.INTEGER,
      allowNull: true,
      primaryKey: true,
      field: 'id_hairdresser'
    },
    libelle: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'libelle'
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'price'
    },
    duration: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'duration'
    }
  }, {
    tableName: 'prestation'
  });
};
