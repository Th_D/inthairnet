/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('booking', {
    idPrestation: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id_prestation'
    },
    idClient: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id_client'
    },
    date_booking: {
      type: DataTypes.DATE,
      primaryKey: true,
      field: 'date_booking'
    }
  }, {
    tableName: 'booking'
  });
};
