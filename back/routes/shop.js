const models = require("../models");
require('./authentification');
const { checkSchema } = require('express-validator');
const { check, validationResult } = require('express-validator');

module.exports = (db, router) => {
    

    /**
     * @typedef Shop
     * @property {integer} idHairdresser - Manager of the shop
     * @property {string} address.required - address of the shop
     * @property {string} city.required - city of the shop
     * @property {string} name.required - Name of the shop
     * @property {object} openningHours.required - openning hours per day of the week
     */

    /**
     * This function create a shop
     * @route POST /shop
     * @group Shop - Operations about shop
     * @param {Shop.model} shop.body.required - the new shop
     * @returns {object} 200 - An array of shop
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
     */
    router.post('/shop',
        checkSchema({
            name: {
                in: ['params', 'body'],
                errorMessage: 'name is wrong',
                isString: 'true',
                isLength: {
                    errorMessage: 'name should be at least 3 chars long',
                    options: { min: 3 }
                }
            },
            address: {
                in: ['params', 'body'],
                errorMessage: 'address is wrong',
                isString: 'true',
                isLength: {
                    errorMessage: 'address is wrong',
                    options: { min: 2 }
                }
            },
            city: {
                in: ['params', 'body'],
                errorMessage: 'city is wrong',
                isString: 'true',
                isLength: {
                    errorMessage: 'city is wrong',
                    options: { min: 2 }
                }
            },
            openningHours: {
                in: ['params', 'body'],
                errorMessage: 'openningHours is wrong'
            }

        }),
        (req, res) => {
            console.log("POST shop [ " , req.body , " ]");
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            }
            return models.shop.create({
                name: req.body.name,
                address: req.body.address,
                city: req.body.city,
                openningHours: req.body.openningHours
            }).then(function (shop) {
                if (shop && idHairdresser) {
                    // console.log("Current user : ",idHairdresser); 
                    // On set le shop au coiffeur
                    var body = {idShop:shop.id, isManager:true};
                    models.hairdresser.update(body, {
                        where: {
                            id: idHairdresser
                        }
                    });
                    for (let entry of req.body.hairdressers) {
                        var body2 = {idShop:shop.id, isManager:false};
                        models.hairdresser.update(body2, {
                            where: {
                                id: entry.id
                            }
                        });
                    }
                    res.send(shop);
                } else {
                    res.status(400).send('Error in insert new record');
                }
            });
        });

    /* GET */

    /**
    * This function return some shops
    * @route GET /shop
    * @group Shop - Operations about shop
    * @returns {object} 200 - An array of shop
    * @returns {Error}  default - Unexpected error
    * @security bearerAuth
    */
    router.get('/shop', async (req, res) => {
        
        console.log("GET shop");
        models.shop.findAll().then((shops) => {
            res.send(shops);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });

    /**
     * This function return a shop by id
     * @route GET /shop/{id}
     * @group Shop - Operations about shop
     * @param {integer} id.path.required
     * @returns {object} 200 - information about a shop
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
     */
    router.get('/shop/:id',
        checkSchema({
            id: {
                in: ['params', 'path'],
                isInt: 'true',
                errorMessage: 'id is wrong'
            }
        }),
        (req, res) => {            
            var shopid = req.params.id;
            console.log("GET shop [id=" + shopid + "]");
            models.shop.findByPk(shopid).then((shop) => {
                res.send(shop);
            }).catch((error) => {
                console.log(error);
                res.status(500).send("Error in retreiving data");
            });
        });



    /* PUT */

    /**
     * This function update a shop
     * @route PUT /shop/{id}
     * @group Shop - Operations about shop
     * @param {integer} id.path.required
     * @returns {object} 200 - a shop updated
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
     */
    router.put('/shop/:id', (req, res) => {
        
        console.log("PUT shop [id=" + req.params.id + "]");
        // delete what shouldn't be updatable
        // delete req.body.title;
        models.shop.update(req.body, {
            where: {
                id: req.params.id
            }
        }).then((shop) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });


    /* DELETE */

    /**
     * This function delete a shop by this id
     * @route DELETE /shop/{id}
     * @group Shop - Operations about shop
     * @param {integer} id.path.required
     * @returns {object} 200 - shop was deleted
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
     */
    router.delete('/shop/:id', (req, res) => {
        console.log("DELETE shop [id=" + req.params.id + "]");
        models.shop.destroy({
            where: {
                id: req.params.id
            }
        }).then((shop) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });


}
