const models = require("../models");
const config = require('config.json');
const jwt = require('jsonwebtoken');

module.exports = (db, router) => {

    
    /* POST */
    //TODO: validate objects incoming for insertion https://express-validator.github.io/docs/

    /**
     * @typedef AuthAttempt
     * @property {string} mail.required - mail attached to the account
     * @property {string} pwd.required - password of the user
     */

    /**
     * try to login
     * @route post /authenticate
     * @group Authentication - Operations about prestation
     * @param {AuthAttempt.model} auth.body.required - Authentication data (mail and password of the user who try to connect)
     * @returns {integer} 200 - id of the user
     * @returns {Error}  500 - Unexpected error
    */
    router.post('/authenticate', async (req, res) => {
        console.log("POST Authentication");

        if (req.body.mail == "admin" && req.body.pwd == "admin"){
            console.log("Authentificate admin mode")
            const token = jwt.sign({ sub: 0}, config.secret);
            var connexion = {id:-1, token:token};
            res.status(200).send(connexion);
        }
        else{
            reqq = 'SELECT HAIRDRESSER.ID FROM AUTHENTICATION, HAIRDRESSER \
            WHERE HAIRDRESSER.ID_AUTHENTICATION = AUTHENTICATION.ID \
            AND HAIRDRESSER.EMAIL = \''+ req.body.mail + '\'\
            AND PASSWORD =\''+ req.body.pwd + '\'; ';

            await db.sequelize.query(reqq).spread(function (results, metadata) {
                console.log("Authentificate => ", results)
                if(metadata.rowCount == 1){
                    idHairdresser = results[0].id;
                    // res.status(200).send(true);
                    const token = jwt.sign({ sub: results[0].id}, config.secret);
                    var connexion = {id:results[0].id, token:token};
                    res.status(200).send(connexion);
                } else{
                    //Not found or > 1 cpt with this mail
                    // res.status(200).send(false);
                    res.status(500).send("Error in retreiving data");
                }
            });
        }
        
    });

        /**
    * This function return some auth
    * @route GET /auth
    * @group Authentication - Operations about auth
    * @returns {object} 200 - An array of auth
    * @returns {Error}  default - Unexpected error
    * @security bearerAuth
    */
   router.get('/auth', async (req, res) => {
        console.log("GET auth");
        models.authentication.findAll().then((authentications) => {
            res.send(authentications);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });

}
