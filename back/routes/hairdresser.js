const models = require("../models");
const { checkSchema } = require('express-validator');
const { check, validationResult } = require('express-validator');

module.exports = (db, router) => {

    /**
     * @typedef Hairdresser
     * @property {integer} idShop - address of the shop
     * @property {boolean} isManager - address of the shop
     * @property {string} lastname.required - city of the shop
     * @property {string} firstname.required - Name of the shop
     * @property {object} specialities.required - openning hours per day of the week
     * @property {string} email.required - Name of the shop
     * @property {string} phoneNumber.required - Name of the shop
     * @property {object} disponibilities - Name of the shop
     * @property {string} leaves - Name of the shop
    */

    /**
     * @typedef Registration
     * @property {integer} idShop - address of the shop
     * @property {boolean} isManager - address of the shop
     * @property {string} lastname.required - city of the shop
     * @property {string} firstname.required - Name of the shop
     * @property {object} specialities.required - openning hours per day of the week
     * @property {string} email.required - Name of the shop
     * @property {string} phoneNumber.required - Name of the shop
     * @property {object} disponibilities - Name of the shop
     * @property {string} leaves - Name of the shop
     * @property {string} password - Password of the user
    */

    /**
     * This function create a hairdresser
     * @route POST /hairdresser
     * @group Hairdresser - Operations about hairdresser
     * @param {Registration.model} hairdresser.body.required - the new hairdresser
     * @returns {object} 200 - An array of hairdresser
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.post('/hairdresser',
        checkSchema({
            lastname: {
                in: ['params', 'body'],
                errorMessage: 'lastname is wrong',
                isString: 'true'
            },
            firstname: {
                in: ['params', 'body'],
                errorMessage: 'firstname is wrong',
                isString: 'true',
                isLength: {
                    errorMessage: 'firstname is wrong',
                    options: { min: 3 }
                }
            },
            email: {
                in: ['params', 'body'],
                errorMessage: 'mail is wrong',
                isEmail: {
                    errorMessage: 'email is not a valid mail',
                }
            },
            phoneNumber: {
                in: ['params', 'body'],
                errorMessage: 'phoneNumber is wrong',
                isString: 'true',
                isLength: {
                    errorMessage: 'firstname is wrong',
                    options: { min: 10 }
                }
            },
            password: {
                in: ['params', 'body'],
                errorMessage: 'password is wrong',
                isString: 'true',
                isLength: {
                    errorMessage: 'firstname is wrong',
                    options: { min: 3 } // change after test
                }
            }
        }),
        (req, res) => {
            console.log("POST hairdresser [", req.body, "]");
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            }

            models.authentication.create({
                password: req.body.password
            })
                .then(function (authId) {
                    return models.hairdresser.create({
                        idAuthentication: authId['dataValues']['id'],
                        idShop: req.body.idShop,
                        isManager: req.body.isManager,
                        lastname: req.body.lastname,
                        firstname: req.body.firstname,
                        specialities: req.body.specialities,
                        email: req.body.email,
                        phoneNumber: req.body.phoneNumber,
                        disponibilities: req.body.disponibilities,
                        leaves: req.body.leaves
                    }).then(function (hairdresser) {
                        if (hairdresser) {
                            console.log("Hairdedder Created");
                            res.send(hairdresser);
                        } else {
                            res.status(400).send('Error in insert new record');
                        }
                    }).catch(function (err) {
                        res.status(400).send('Error in insert new record');
                    });
                });
        });

    /**
    * This function return some hairdressers
    * @route GET /hairdresser
    * @group Hairdresser - Operations about hairdresser
    * @returns {object} 200 - An array of hairdresser
    * @returns {Error}  default - Unexpected error
    * @security bearerAuth
    */
    router.get('/hairdresser', async (req, res) => {
        console.log("GET hairdresser");
        models.hairdresser.findAll().then((hairdressers) => {
            res.send(hairdressers);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });

    /**
     * This function return a hairdresser by id
     * @route GET /hairdresser/{id}
     * @group Hairdresser - Operations about hairdresser
     * @param {integer} id.path.required
     * @returns {object} 200 - information about a hairdresser
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.get('/hairdresser/:id', (req, res) => {
        var hairdresserid = req.params.id;
        console.log("GET hairdresser [id=" + hairdresserid + "]");
        models.hairdresser.findByPk(hairdresserid).then((hairdresser) => {
            res.send(hairdresser);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });


    /* PUT */

    /**
     * This function update a hairdresser
     * @route PUT /hairdresser/{id}
     * @group Hairdresser - Operations about hairdresser
     * @param {integer} id.path.required
     * @returns {object} 200 - a hairdresser updated
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.put('/hairdresser/:id', (req, res) => {
        console.log("PUT hairdresser [id=" + req.params.id + "]");
        // delete what shouldn't be updatable
        // delete req.body.title;
        models.hairdresser.update(req.body, {
            where: {
                id: req.params.id
            }
        }).then((hairdresser) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });


    /* DELETE */

    /**
     * This function delete a hairdresser by this id
     * @route DELETE /hairdresser/{id}
     * @group Hairdresser - Operations about hairdresser
     * @param {integer} id.path.required
     * @returns {object} 200 - hairdresser was deleted
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.delete('/hairdresser/:id', (req, res) => {
        console.log("DELETE hairdresser [id=" + req.params.id + "]");
        models.hairdresser.destroy({
            where: {
                id: req.params.id
            }
        }).then((hairdresser) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });


}
