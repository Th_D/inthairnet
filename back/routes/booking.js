const models = require("../models");
const { checkSchema } = require('express-validator');
const { check, validationResult } = require('express-validator');

module.exports = (db, router) => {

    /**
     * @typedef Booking
     * @property {integer} idPrestation - address of the shop
     * @property {integer} idClient - city of the shop
     * @property {string} date_booking - Name of the shop
    */

    /**
     * This function create a hairdresser
     * @route POST /booking
     * @group Booking - Operations about bookings
     * @param {Booking.model} booking.body.required - the new booking
     * @returns {object} 200 - success
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.post('/booking',
        checkSchema({
            idPrestation: {
                in: ['params', 'body'],
                errorMessage: 'idPrestation is wrong',
                isInt: 'true'
            },
            idClient: {
                in: ['params', 'body'],
                errorMessage: 'idClient is wrong',
                isInt: 'true'
            },
            dateBooking: {
                in: ['params', 'body'],
                errorMessage: 'dateBooking is wrong'
            }
        }),
        (req, res) => {
            console.log("POST booking [", req.body, "]");
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            }
            console.log(req.body.date_booking);
            models.booking.create({
                idPrestation: req.body.idPrestation,
                idClient: req.body.idClient,
                date_booking: req.body.date_booking,
            }).then(function (booking) {
                if (booking) {
                    res.send(booking);
                } else {
                    res.status(400).send('Error in insert new record');
                }
            }).catch(function (err) {
                res.status(400).send(err);
            });
        });

    /**
    * This function return some bookings
    * @route GET /booking
    * @group Booking - Operations about bookings
    * @returns {object} 200 - An array of bookings
    * @returns {Error}  default - Unexpected error
    * @security bearerAuth
    */
    router.get('/booking', async (req, res) => {
        console.log("GET booking");
        models.booking.findAll().then((bookings) => {
            res.send(bookings);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });

    /**
     * This function return bookings of a specific hairdresser
     * @route GET /booking/{hairdresser}
     * @group Booking - Operations about bookings
     * @param {integer} hairdresser.path.required
     * @returns {object} 200 - information about a hairdresser
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.get('/booking/:hairdresser', async (req, res) => {

        console.log("GET booking [id=" + req.params.hairdresser + "]");
        req = 'SELECT * \
        FROM BOOKING, PRESTATION \
        WHERE BOOKING.id_prestation = PRESTATION.id \
        AND PRESTATION.id_hairdresser = \''+ req.params.hairdresser + '\'';

        await db.sequelize.query(req).then(function (results, metadata) {
            if (results) {
                console.log(results[0]);
                res.status(200).send(results[0]);
            }
            else {
                console.log("ERROR " + req.body.mail);
                res.status(500).send("Error in retreiving data");
            }
        });
    });


    /**
     * This function return bookings of a specific hairdresser
     * @route GET /booking/{hairdresser}/{year}/{month}
     * @group Booking - Operations about bookings
     * @param {integer} hairdresser.path.required
     * @param {integer} year.path.required
     * @param {integer} month.path.required
     * @returns {object} 200 - information about a hairdresser
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
    router.get('/booking/:hairdresser/:year/:month', async (req, res) => {

        console.log("GET booking [id=" + req.params.hairdresser + "]");
        req = 'SELECT * \
    FROM BOOKING, PRESTATION \
    WHERE BOOKING.id_prestation = PRESTATION.id \
    AND EXTRACT(MONTH FROM date_booking) = \''+ req.params.month + '\' \
    AND EXTRACT(YEAR FROM date_booking) = \''+ req.params.year + '\' \
    AND PRESTATION.id_hairdresser = \''+ req.params.hairdresser + '\'';

        await db.sequelize.query(req).then(function (results, metadata) {
            if (results) {
                console.log(results[0]);
                res.status(200).send(results[0]);
            }
            else {
                console.log("ERROR " + req.body.mail);
                res.status(500).send("Error in retreiving data");
            }
        });
    });


    
    /**
     * This function delete bookings
     * @route DELETE /booking/{idPrestation}/{idClient}/{date_booking}
     * @group Booking - Operations about bookings
     * @param {integer} idPrestation.path.required
     * @param {integer} idClient.path.required
     * @param {string} date_booking.path.required
     * @returns {object} 200 - information about a booking
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
    */
   router.delete('/booking/:idPrestation/:idClient/:date_booking', async (req, res) => {

        console.log("DELETE booking [", req.params, "]");

        // query_abc = 'DELETE \
        // FROM BOOKING \
        // WHERE id_prestation = \''+ req.params.idPrestation + '\' \
        // AND id_client = \''+ req.params.idClient + '\' \
        // AND date_booking = \''+ req.params.date_booking + '\'';

        // await db.sequelize.query(query_abc).then(function (results, metadata) {
        //     if (results) {
        //         console.log(results[0]);
        //         res.status(200).send(results[0]);
        //     }
        //     else {
        //         console.log("ERROR ");
        //         res.status(500).send("Error in retreiving data");
        //     }
        // });

        models.booking.destroy({
            where: {
                id_prestation: req.params.idPrestation,
                id_client: req.params.idClient,
                date_booking: req.params.date_booking
            }
        }).then((booking) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });
}