const models = require("../models");

module.exports = (db, router) => {
    /* POST */
    //TODO: validate objects incoming for insertion https://express-validator.github.io/docs/

    /**
     * @typedef Prestation
     * @property {integer} idHairdresser.required - id of the hairdresser attached to the prestation
     * @property {string} libelle.required - Some description for product
     * @property {float} price.required - Price of the prestation (€)
     * @property {integer} duration.required - Duration of the prestation (minutes)
     */

    /**
     * Creation of a prestation
     * @route POST /prestation
     * @group Prestation - Operations about prestation
     * @param {Prestation.model} prestation.body.required - The prestation to create
     * @returns {object} 200 - An array of prestation
     * @returns {Error}  default - Unexpected error
     * @security bearerAuth
     */
    router.post('/prestation', (req, res) => {
        console.log("POST prestation [" + req.body + "]");
        return models.prestation.create(req.body)
            .then(function (prestation) {
                if (prestation) {
                    res.send(prestation);
                } else {
                    res.status(400).send('Error in insert new record');
                }
            });
    });


    /* GET */

    // /prestation
    /**
     * Get all prestations
     * @route GET /prestation
     * @group Prestation - Operations about prestation
     * @returns {object} 200 - An array of prestation
     * @returns {Error}  500 - Unexpected error
     * @security bearerAuth
     */
    router.get('/prestation', async (req, res) => {
        console.log("GET prestation");
        models.prestation.findAll().then((prestations) => {
            res.send(prestations);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });

    /**
     * Get a prestation by id
     * @route GET /prestation/{id}
     * @group Prestation - Operations about prestation
     * @param {integer} id.path.required
     * @returns {object} 200 - The prestation with specified id
     * @returns {Error}  500 - Unexpected error
     * @security bearerAuth
     */
    router.get('/prestation/:id', (req, res) => {
        var prestationid = req.params.id;
        console.log("GET prestation [id=" + prestationid + "]");
        models.prestation.findByPk(prestationid).then((prestation) => {
            res.send(prestation);
        }).catch((error) => {
            console.log(error);
            res.status(500).send("Error in retreiving data");
        });
    });


    /* PUT */

    /**
     * Modify a prestation by id
     * @route PUT /prestation/{id}
     * @group Prestation - Operations about prestation
     * @param {integer} id.path.required
     * @returns {object} 200 - true
     * @returns {Error}  500 - Unexpected error
     * @security bearerAuth
     */
    router.put('/prestation/:id', (req, res) => {
        console.log("PUT prestation [id=" + req.params.id + "]");
        // delete what shouldn't be updatable
        // delete req.body.title;
        models.prestation.update(req.body, {
            where: {
                id: req.params.id
            }
        }).then((prestation) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });


    /* PATCH */

    /* DELETE */

    /**
     * Delete a prestation by id
     * @route DELETE /prestation/{id}
     * @group Prestation - Operations about prestation
     * @param {integer} id.path.required
     * @returns {object} 200 - true
     * @returns {Error}  500 - Unexpected error
     * @security bearerAuth
     */
    router.delete('/prestation/:id', (req, res) => {
        console.log("DELETE prestation [id=" + req.params.id + "]");
        models.prestation.destroy({
            where: {
                id: req.params.id
            }
        }).then((prestation) => {
            res.status(200).send(true);
        }).catch((error) => {
            res.sendStatus(500);
        });
    });


}
