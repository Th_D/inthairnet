  
const expressJwt = require('express-jwt');
const config = require('config.json');

module.exports = jwt;

function jwt() {
    const { secret } = config;
    return expressJwt({ secret }).unless({
        path: [
            // public routes that don't require authentication
            '/api-docs',
            '/api-docs/swagger-ui.css',
            '/api-docs/swagger-ui-bundle.js',
            '/api-docs/swagger-ui-standalone-preset.js',
            '/api-docs.json',
            '/api-docs/favicon-32x32.png',
            '/api-docs/favicon-16x16.png',

            '/api/v1/hairdresser', // to create a hairdresser
            '/api/v1/authenticate', // to authentificate
        ]
    });
}