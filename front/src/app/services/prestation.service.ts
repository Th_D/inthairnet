import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Prestation } from './../models/prestation';
import { Observable, of } from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class PrestationService {
  apiUrl = 'http://localhost:3000/api/v1/prestation';

  constructor(private http: HttpClient) { }

  getPrestation(id: number | string) : Observable<Prestation> {
    return this.http.get<Prestation>(`${this.apiUrl}/${id}`);
  }

  getPrestations() : Observable<Prestation[]> {
    return this.http.get<Prestation[]>(this.apiUrl);
  }

  createPrestation(prestation: Prestation): Observable<any> {
    console.log("[SERVICE] + ", prestation);
    return this.http.post<Prestation>(this.apiUrl, prestation).pipe(
      retry(3), catchError(this.handleError<Prestation>('createPrestation')));
  }

  updatePrestation(prestation: Prestation) : Observable<any>  {
    return this.http.put(`${this.apiUrl}/${prestation.id}`, prestation);
  }

  deletePrestation(id: number | string) : Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/${id}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
