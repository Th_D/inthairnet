import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hairdresser } from './../models/hairdresser';
import { Observable, of } from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';


@Injectable({
  providedIn: 'root'
})
export class HairdresserService {
  apiUrl = 'http://localhost:3000/api/v1/hairdresser';
  constructor(private http: HttpClient) { }

  createHaird(hairdresser: Hairdresser): Observable<any> {
    console.log("[SERVICE] + ", hairdresser);
    return this.http.post<Hairdresser>(this.apiUrl, hairdresser).pipe(
      retry(3), catchError(this.handleError<Hairdresser>('createHairdresser')));
  }

  getHairds() : Observable<Hairdresser[]> {
    return this.http.get<Hairdresser[]>(this.apiUrl);
  }

  getHaird(id: number | string) : Observable<Hairdresser> {
    return this.http.get<Hairdresser>(`${this.apiUrl}/${id}`);
  }

  updateHaird(hairdresser: Hairdresser) : Observable<any>  {
    return this.http.put(`${this.apiUrl}/${hairdresser.id}`, hairdresser);
  }

  deleteHaird(id: number | string) : Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/${id}`);
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }


}
