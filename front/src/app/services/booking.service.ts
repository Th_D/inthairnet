import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Booking } from './../models/booking';
import { Observable, of } from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  apiUrl = 'http://localhost:3000/api/v1/booking';

  constructor(private http: HttpClient) { }

  // getBooking(id: number | string) : Observable<Booking> {
  //   return this.http.get<Booking>(`${this.apiUrl}/${id}`);
  // }

  getBookings() : Observable<Booking[]> {
    return this.http.get<Booking[]>(this.apiUrl);
  }

  getBookingsByHairdresser(id: number | string) : Observable<Booking[]> {
    return this.http.get<Booking[]>(`${this.apiUrl}/${id}`);
  }

  createBooking(booking: Booking): Observable<any> {
    console.log("[SERVICE] + ", booking);
    return this.http.post<Booking>(this.apiUrl, booking).pipe(
      retry(3), catchError(this.handleError<Booking>('createBooking')));
  }

  // updateBooking(booking: Booking) : Observable<any>  {
  //   return this.http.put(`${this.apiUrl}/${booking.id}`, booking);
  // }

  deleteBooking(id: number | string) : Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/${id}`);
  }

  deleteBooking2(idPrestation: number, idClient: number, date_booking: Date) : Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/${idPrestation}/${idClient}/${date_booking}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
