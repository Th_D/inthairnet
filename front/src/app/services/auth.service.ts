import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';
import { User } from './../models/user';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl = 'http://localhost:3000/api/v1/auth';

  private currentUserSubject: BehaviorSubject<Object>;
  public currentUser: Observable<Object>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Object>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Object {
    return this.currentUserSubject.value;
  }



  // login(user:User) {
  //   return this.http.post<any>(this.apiUrl, user)
  //       .pipe(map(co => {
  //           // store user details and jwt token in local storage to keep user logged in between page refreshes
  //           // console.log("[SERVICE LOGIN => " , user.mail);
  //           if (co){
  //             // localStorage.setItem('currentUser', JSON.stringify(user.mail));
  //             // this.currentUserSubject.next(user.mail);
  //           }
  //           return co;
  //       }));
  // }

  login(user:User) {
    return this.http.post<any>(`http://localhost:3000/api/v1/authenticate`, user)
        .pipe(map(ret_user => {
            console.log(ret_user);
            // login successful if there's a jwt token in the response
            if (ret_user && ret_user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(ret_user));
                // console.error(ret_user, ret_user[0].id, ret_user['id'])
                this.currentUserSubject.next(ret_user); //TODO
            }

            return ret_user;
        }));
}

  // loginAdmin(user:User) {
  //   var admin = {id:0, token:"admin"};
  //   localStorage.setItem('currentUser', JSON.stringify(admin));
  //   this.currentUserSubject.next(admin.token);    
  //   return true;
  // }

  logout() {
    // remove user from local storage and set current user to null

    console.error("service logout")
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['user-signin']);

  }

  // connect(user: User): Observable<boolean> {
  //   // console.log("[SERVICE]  +>  mail = ", user.mail, "  |  pwd = ", user.pwd);
  //   return this.http.post<boolean>(this.apiUrl, user).pipe(
  //     retry(3), catchError(this.handleError<boolean>('try connection')));
  // }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
