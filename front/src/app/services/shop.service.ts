import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Shop } from './../models/shop';
import { Observable, of } from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';


@Injectable({
  providedIn: 'root'
})
export class ShopService { 
  apiUrl = 'http://localhost:3000/api/v1/shop';

  constructor(private http: HttpClient) { }

  // TUTORIEL SUIVI ; https://www.djamware.com/post/5d8d7fc10daa6c77eed3b2f2/angular-8-tutorial-rest-api-and-httpclient-examples

  getShops() : Observable<Shop[]> {
    return this.http.get<Shop[]>(this.apiUrl);
  }

  getShop(id: number | string) : Observable<Shop> {
    return this.http.get<Shop>(`${this.apiUrl}/${id}`);
  }

  createShop(shop: Shop): Observable<any> {
    console.log("[SERVICE] + ", shop);

    return this.http.post<Shop>(this.apiUrl, shop).pipe(
      retry(3), catchError(this.handleError<Shop>('createShop')));
  }

  updateShop(shop: Shop) : Observable<any>  {
    return this.http.put(`${this.apiUrl}/${shop.id}`, shop);
  }


  deleteShop(id: number | string) : Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/${id}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
  
}