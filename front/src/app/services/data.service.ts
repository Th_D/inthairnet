import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  shops = [ 
    {id: 1, name: "Shop 001", description: "Shop 001 des", email: "Shop001@email.com"},
    {id: 2, name: "Shop 002", description: "Shop 002 des", email: "Shop002@email.com"},
    {id: 3, name: "Shop 003", description: "Shop 003 des", email: "Shop003@email.com"},
    {id: 4, name: "Shop 004", description: "Shop 004 des", email: "Shop004@email.com"}
  ];

  constructor() { }

  public getShops():Array<{id, name, description, email}>{
    return this.shops;
  }
  public createShop(shop: {id, name, description, email}){
    this.shops.push(shop);
  }
}
