import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './interceptors/HttpErrorInterceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeAdminComponent } from './views/home/home-admin/home-admin.component';
import { HomeUserComponent } from './views/home/home-user/home-user.component';
import { ShopCreateComponent } from './views/shop/shop-create/shop-create.component';
import { ShopListComponent } from './views/shop/shop-list/shop-list.component';
import { ShopDetailsComponent } from './views/shop/shop-details/shop-details.component';
import { HeaderComponent } from './views/header/header.component';
import { FooterComponent } from './views/footer/footer.component';
import { UserSigninComponent } from './views/login/user-signin/user-signin.component';
import { UserSignupComponent } from './views/login/user-signup/user-signup.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { HairdresserCreateComponent } from './views/hairdresser/hairdresser-create/hairdresser-create.component';
import { HairdresserListComponent } from './views/hairdresser/hairdresser-list/hairdresser-list.component';
import { HairdresserDetailsComponent } from './views/hairdresser/hairdresser-details/hairdresser-details.component';

import { NgxMaskModule } from 'ngx-mask';

// used to create fake backend
import { fakeBackendProvider } from 'src/app/helpers/fake-backend';
import { JwtInterceptor } from 'src/app/helpers/jwt.interceptor';
import { ErrorInterceptor } from 'src/app/helpers/error.interceptor';
import { ShopManageComponent } from './views/shop/shop-manage/shop-manage.component';
import { HairdresserManageComponent } from './views/hairdresser/hairdresser-manage/hairdresser-manage.component';
import { PrestationManageComponent } from './views/prestation/prestation-manage/prestation-manage.component';
import { BookingManageComponent } from './views/booking/booking-manage/booking-manage.component';

import { jqxBarGaugeModule }    from 'jqwidgets-ng/jqxbargauge';
import{ jqxSchedulerModule } from 'jqwidgets-ng/jqxscheduler';
import { jqxMenuModule } from 'jqwidgets-ng/jqxmenu';
import { PrestationListComponent } from './views/prestation/prestation-list/prestation-list.component';
import { BookingListComponent } from './views/booking/booking-list/booking-list.component';
import { BookingDetailsComponent } from './views/booking/booking-details/booking-details.component'; 

@NgModule({
  declarations: [
    AppComponent,
    HomeAdminComponent,
    HomeUserComponent,
    ShopCreateComponent,
    ShopListComponent,
    ShopDetailsComponent,
    HeaderComponent,
    FooterComponent,
    UserSigninComponent,
    UserSignupComponent,
    HairdresserCreateComponent,
    HairdresserListComponent,
    HairdresserDetailsComponent,
    HomeUserComponent,
    ShopManageComponent,
    HairdresserManageComponent,
    PrestationManageComponent,
    BookingManageComponent,
    PrestationListComponent,
    BookingListComponent,
    BookingDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    NgxMaterialTimepickerModule,
    NgxMaskModule.forRoot(),
    jqxBarGaugeModule,
    jqxSchedulerModule,
    jqxMenuModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    // fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
