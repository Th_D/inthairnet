import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/services/booking.service';
import swal from 'sweetalert2';
declare var jQuery: any;

function filter_tab($) {
  $(document).ready(function(){
    $("#demo").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#test tr").filter(function() {
         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
   });
  });
}
@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.scss']
})
export class BookingListComponent implements OnInit {

  bookings;
  selectedbooking;

  constructor(public bookingService: BookingService) { }

  ngOnInit() {
    this.bookings = this.getbookings();
    filter_tab(jQuery);
  }

  public selectbooking(booking){
    this.selectedbooking = booking;
  }

  getbookings() {
    this.bookingService.getBookings().subscribe(bookings => {
      this.bookings = bookings;
      console.log(this.bookings);
    });
  }

  public deletebooking(booking){
    console.log("Delete booking ID = ",booking.idPrestation);
    this.bookingService.deleteBooking2(booking.idPrestation, booking.idClient, booking.date_booking).subscribe(bookings => {
      swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'The booking has been deleted successfully',
        showConfirmButton: false,
        timer: 1500
      });
      this.getbookings();
    },
    err => {
      swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'The booking cannot be deleted due to use (booking)',
        text: 'Something went wrong!',
    })
    });
  }

}