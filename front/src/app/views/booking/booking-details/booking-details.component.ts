
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, AfterViewInit } from '@angular/core';
 
import { jqxSchedulerComponent } from 'jqwidgets-ng/jqxscheduler';
import { BookingService } from 'src/app/services/booking.service';
import { AuthService } from 'src/app/services/auth.service';
import { Booking } from 'src/app/models/booking';
@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss']
})
export class BookingDetailsComponent implements AfterViewInit {
  idHairdresser:number;
  bookings:Booking[];
  mydata: any[] = [];
  constructor(private authService: AuthService, private bookingService: BookingService, private route: ActivatedRoute, private router: Router) { }


  @ViewChild('schedulerReference') scheduler: jqxSchedulerComponent;
    ngAfterViewInit(): void {
        this.scheduler.ensureAppointmentVisible('id1');

        // var currentId = this.authService.currentUser.source['_value']['id'];

        this.route.params.subscribe(params => {
          this.idHairdresser = params['id'];
          console.log(this.idHairdresser);

        let appointments = new Array();
        this.bookingService.getBookingsByHairdresser(this.idHairdresser).subscribe(data => {
            console.log("[bookingService] => ",data);
            this.bookings = data;

            let i = 0;
            for (let item of this.bookings) {
                var start_date = new Date(item['date_booking']);
                var end_date = new Date();
                end_date.setTime(start_date.getTime() + (item['duration'] * 60 * 1000));
                var start_str =  start_date.getHours()+"h"+ start_date.getMinutes();
                var end_str =  end_date.getHours()+"h"+ end_date.getMinutes(); 
                var desc = start_str+" - "+ end_str
                let appointment1 = {
                    id: "id_rdv"+i,
                    description: desc,
                    location: "1 Rue du coiffeur",
                    subject: item['libelle'],
                    calendar: "DORIAN VERRIERE"+i,
                    start: start_date,
                    end: end_date
                };
                appointments.push(appointment1);
                i++;
              }
              this.mydata = appointments;
              this.source.localData = this.mydata;
              this.dataAdapter = new jqx.dataAdapter(this.source)
        });
      });
        
    }
 
    source: any =
    {
        dataType: "array",
        dataFields: [
            { name: 'id', type: 'string' },
            { name: 'description', type: 'string' },
            { name: 'location', type: 'string' },
            { name: 'subject', type: 'string' },
            { name: 'calendar', type: 'string' },
            { name: 'start', type: 'date' },
            { name: 'end', type: 'date' }
        ],
        id: 'id',
        localData: this.mydata
    };
    dataAdapter: any = new jqx.dataAdapter(this.source);
    date: any =  new jqx.date(new Date(Date.now()).getFullYear(), new Date(Date.now()).getMonth()+1, new Date(Date.now()).getDate());
    appointmentDataFields: any =
    {
        from: "start",
        to: "end",
        id: "id",
        description: "description",
        location: "location",
        subject: "subject",
        resourceId: "subject"
    };
    resources: any =
    {
        colorScheme: "scheme06",
        dataField: "subject",
        source: new jqx.dataAdapter(this.source)
    };
    views: any[] =
    [
        'dayView',
        'weekView',
        'monthView'
    ];
    
}

