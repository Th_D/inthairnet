import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import swal from 'sweetalert2'; 
@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.scss']
})
export class UserSignupComponent implements OnInit {

  registerForm = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl(''),
    displayname: new FormControl(''),
    email: new FormControl(''),
    passwd: new FormControl(''),
    passwd_verif: new FormControl(''),
  });

  constructor() { }

  ngOnInit() {
  }

  register(){
    console.log(this.registerForm.value);
    console.log(this.registerForm.value.firstname); // TO GET firstname attribute
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong about the register!'
    })
  }

}
