import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-user-signin',
  templateUrl: './user-signin.component.html',
  styleUrls: ['./user-signin.component.scss']
})
export class UserSigninComponent implements OnInit {

  loginForm = new FormGroup({
    mail: new FormControl(''),
    pwd: new FormControl('')
  });

  constructor(public authService: AuthService, private router : Router) { }

  ngOnInit() {
  }

  signIn() {

    this.authService.login(this.loginForm.value).subscribe(
        data => {
            swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'You are connected',
                showConfirmButton: false,
                timer: 1500
            })
            setTimeout(() => {
                if (data['id'] === -1){
                    this.router.navigate(['home-admin']);
                } else {
                  this.router.navigate(['home-user']);
                }
            }, 1750);
        },
        err => {
            console.log("ERROR : ", err)
            swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Mail or password not found',
                text: 'Something went wrong!',
            })
        }
    );
 
  }
}


// import { Component, OnInit } from '@angular/core';
// import { Router, ActivatedRoute } from '@angular/router';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { first } from 'rxjs/operators';

// import { AuthenticationService } from 'src/app/services/authentication.service';

// @Component({
//   selector: 'app-user-signin',
//   templateUrl: './user-signin.component.html',
//   styleUrls: ['./user-signin.component.scss']
// })
// export class UserSigninComponent implements OnInit {
//     loginForm: FormGroup;
//     loading = false;
//     submitted = false;
//     returnUrl: string;
//     error = '';

//     constructor(
//         private formBuilder: FormBuilder,
//         private route: ActivatedRoute,
//         private router: Router,
//         private authenticationService: AuthenticationService) {}

//     ngOnInit() {
//         this.loginForm = this.formBuilder.group({
//             mail: ['', Validators.required],
//             pwd: ['', Validators.required]
//         });

//         // reset login status
//         this.authenticationService.logout();

//         // get return url from route parameters or default to '/'
//         this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
//     }

//     // convenience getter for easy access to form fields
//     get f() { return this.loginForm.controls; }

//     onSubmit() {
//         this.submitted = true;

//         // stop here if form is invalid
//         if (this.loginForm.invalid) {
//             return;
//         }

//         this.loading = true;
//         // this.authenticationService.login(this.f.username.value, this.f.password.value)
//         console.log(this.loginForm.value)
//         this.authenticationService.login(this.loginForm.value)
//             .pipe(first())
//             .subscribe(
//                 data => {
//                     console.log("this.returnUrl == ", this.returnUrl)
//                     this.router.navigate([this.returnUrl]);
//                 },
//                 error => {
//                     this.error = error;
//                     this.loading = false;
//                 });
//     }
// }
