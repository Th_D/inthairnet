import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HairdresserService } from 'src/app/services/hairdresser.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-hairdresser-details',
  templateUrl: './hairdresser-details.component.html',
  styleUrls: ['./hairdresser-details.component.scss']
})
export class HairdresserDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, public hairdService: HairdresserService, private _location: Location) { }

  id;
  haird;

  ngOnInit(): void {
    this.route.params.subscribe(params => { 
      this.id = params['id'];

      this.hairdService.getHaird(this.id).subscribe(haird => {
        console.log(haird);
        this.haird = haird;
      });
    })
  }

  back(){
    this._location.back();
  }
  
}
