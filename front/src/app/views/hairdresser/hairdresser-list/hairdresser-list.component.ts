import { Component, OnInit } from '@angular/core';
import { HairdresserService } from '../../../services/hairdresser.service';
import { AuthService } from '../../../services/auth.service';
import { User } from 'src/app/models/user';
declare var jQuery: any;

function filter_tab($) {
  $(document).ready(function(){
    $("#demo").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      console.error(value)
      $("#test tr").filter(function() {
         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
   });
  });
} 

@Component({ 
  selector: 'app-hairdresser-list',
  templateUrl: './hairdresser-list.component.html',
  styleUrls: ['./hairdresser-list.component.scss']
})
export class HairdresserListComponent implements OnInit {
  
  currentUser;
  hairds;
  selectedHaird;

  constructor(public hairdService: HairdresserService, public authService:AuthService) {
    this.currentUser = this.authService.currentUserValue;
   }

  ngOnInit(): void {
    this.hairds = this.getHairds();
    filter_tab(jQuery);
  }

  public selectHaird(haird){
    this.selectedHaird = haird;
  }

  getHairds() {
    this.hairdService.getHairds().subscribe(hairds => {
      this.hairds = hairds;
      // console.log(this.shops);
    });
  }

  public deleteHaird(haird){
    console.log("Delete Shop ID = ",haird.id);
    this.hairdService.deleteHaird(haird.id).subscribe(shops => {
      // console.log(shops);
      this.getHairds();
    });
  }

}
