import { Component, OnInit } from '@angular/core';
import { HairdresserService } from 'src/app/services/hairdresser.service';
import { Hairdresser } from 'src/app/models/hairdresser';
import { AuthService } from 'src/app/services/auth.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-hairdresser-manage',
  templateUrl: './hairdresser-manage.component.html',
  styleUrls: ['./hairdresser-manage.component.scss']
})
export class HairdresserManageComponent implements OnInit {

  hairdresser : Hairdresser;
  edition: boolean = false;
  constructor(public authService: AuthService, public hairdresserService: HairdresserService) { }

  ngOnInit(): void {
    var currentId = this.authService.currentUser.source['_value']['id'];
    this.hairdresserService.getHaird(currentId).subscribe(data => {
      this.hairdresser = data;  
      console.error("I am ", this.hairdresser)
    })
  }



  onSave(){
    this.edition = false;
    console.error("MAJ => ",this.hairdresser);
    this.hairdresserService.updateHaird(this.hairdresser).subscribe(data => {
      swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'You have successfully updated your profile',
          showConfirmButton: false,
          timer: 1500
      }) 
    }, 
    err => {
      console.error("[LOG PUT]: ", err)
      swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Incorrect Data Format',
          text: 'Something went wrong!',
      })

    });
  }


}
