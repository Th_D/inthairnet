import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormControl, FormArray, FormBuilder, MinLengthValidator, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import swal from 'sweetalert2';
import { HairdresserService } from '../../../services/hairdresser.service';
import { CustomValidators } from 'src/app/custom-validators';
 
 @Component({
  selector: 'app-hairdresser-create',
  templateUrl: './hairdresser-create.component.html',
  styleUrls: ['./hairdresser-create.component.scss']
 })
 export class HairdresserCreateComponent implements OnInit {

 
  constructor(public hairdresserService: HairdresserService, private router: Router, private _fb: FormBuilder) {}
 
  public myForm: FormGroup;
  
  ngOnInit() {
      this.myForm = this._fb.group({
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        email: new FormControl('',[
            Validators.required,
            Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
        phoneNumber: ['', Validators.required],       
        specialities: this._fb.array([
              this.initSkill(), 
        ]),
        password: ['', Validators.required],
        pwd_verif: ['', Validators.required]
    },  {
        // check whether our password and confirm password match
        validator: CustomValidators.passwordMatchValidator
      });
  }

    get email(){
        return this.myForm.get('email')
    }

 
  createHair() {
   console.log(this.myForm.value);
 
   this.hairdresserService.createHaird(this.myForm.value).subscribe(data => {
    console.log("SUCESS ", data);
    if (data) {
     swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'The hairdresser has been saved',
      showConfirmButton: false,
      timer: 1500
     })
     setTimeout(() => {
      this.router.navigate(['home']);
     }, 1750);
    } else {
     swal.fire({
      position: 'top-end',
      icon: 'error',
      title: 'The hairdresser hasn\'t been saved',
      text: 'Something went wrong!',
     })
    }
   });
  }

    initSkill() {
        return this._fb.group({
            item: ['', Validators.required]
        });
    }

    addSkill() {
        const control = <FormArray>this.myForm.controls['specialities'];
        control.push(this.initSkill());
    }

    removeSkill(i: number) {
        const control = <FormArray>this.myForm.controls['specialities'];
        control.removeAt(i);
    }
 
 }