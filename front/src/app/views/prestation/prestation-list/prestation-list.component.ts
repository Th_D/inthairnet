import { Component, OnInit } from '@angular/core';
import { PrestationService } from 'src/app/services/prestation.service';
import swal from 'sweetalert2';
declare var jQuery: any;

function filter_tab($) {
  $(document).ready(function(){
    $("#demo").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#test tr").filter(function() {
         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
   });
  });
}

@Component({
  selector: 'app-prestation-list',
  templateUrl: './prestation-list.component.html',
  styleUrls: ['./prestation-list.component.scss']
})
export class PrestationListComponent implements OnInit {
  
  prestations;
  selectedPrestation;

  constructor(public prestationService: PrestationService) { }

  ngOnInit() {
    this.prestations = this.getPrestations();
    filter_tab(jQuery);
  }

  public selectPrestation(prestation){
    this.selectedPrestation = prestation;
  }

  getPrestations() {
    this.prestationService.getPrestations().subscribe(prestations => {
      this.prestations = prestations;
      console.log(this.prestations);
    });
  }

  public deletePrestation(prestation){
    console.log("Delete Prestation ID = ",prestation.id);
    this.prestationService.deletePrestation(prestation.id).subscribe(prestations => {
      swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'The prestation has been deleted successfully',
        showConfirmButton: false,
        timer: 1500
      });
      this.getPrestations();
    },
    err => {
      swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'The prestation cannot be deleted due to use (booking)',
        text: 'Something went wrong!',
    })
    });
  }

}
