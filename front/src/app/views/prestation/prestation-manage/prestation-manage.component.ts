import { Component, OnInit } from '@angular/core';
import { Prestation } from 'src/app/models/prestation';
import { PrestationService } from 'src/app/services/prestation.service';
import { AuthService } from 'src/app/services/auth.service';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder } from '@angular/forms';
declare var jQuery: any;

function filter_tab($) {
  $(document).ready(function(){
    $("#demo").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#test tr").filter(function() {
         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
   });
  });
} 


@Component({
  selector: 'app-prestation-manage',
  templateUrl: './prestation-manage.component.html',
  styleUrls: ['./prestation-manage.component.scss']
})
export class PrestationManageComponent implements OnInit {

  isLoaded: boolean = false;
  createMode: boolean = true;
  viewMode: boolean = false;
  editMode: boolean = false;
  prestations: Prestation[];
  newPresta: Prestation;
  prestation: Prestation;

  public prestaForm : FormGroup;

  constructor( private prestationService: PrestationService, private authService: AuthService, private _fb: FormBuilder) { }

  ngOnInit(): void {

    var currentId = this.authService.currentUser.source['_value']['id'];
    
    this.prestaForm = this._fb.group({
      idHairdresser: currentId,
      libelle: "",
      price: "",
      duration: "",
    });

    this.prestationService.getPrestations().subscribe(data => {
      this.prestations = data.filter(res => {
        if(res.idHairdresser == currentId){
          return res;
        }
      })
      this.isLoaded = true;
      // console.error("My Prestas", this.prestations)
    });
   
    filter_tab(jQuery);
  }
 

  savePrestation(){
    console.error("To Save => ", this.prestaForm.value);
    this.prestationService.createPrestation(this.prestaForm.value).subscribe(data=>{
      console.log("SAVED",data);
      swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your prestation has been saved successfully',
        showConfirmButton: false,
        timer: 1500
    })
      this.ngOnInit();
    });
  }

  saveModification(){
    console.error("UPDATE => ", this.prestation);
    this.prestationService.updatePrestation(this.prestation).subscribe(data =>{
      this.editMode = false;
      swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your prestation has been updated successfully',
        showConfirmButton: false,
        timer: 1500
    });
    this.ngOnInit();
    }, 
    err => {
      console.error("[LOG PUT]: ", err)
      swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Incorrect Data Format',
          text: 'Something went wrong!',
      })
    });    
  }

  deletePrestation(presta: Prestation){
    console.log("Delete Prestation ID = ",presta.id);
    this.prestationService.deletePrestation(presta.id).subscribe(data => {
      swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your prestation has been deleted successfully',
        showConfirmButton: false,
        timer: 1500
      });
      this.ngOnInit();
    },
    err => {
      console.error("[LOG PUT]: ", err)
      swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'The removal of the prestation failed',
          text: 'Something went wrong!',
      })
    }); 
  }

  showDetail(presta: Prestation){
    this.prestation = presta;
    this.createMode = false;
    this.editMode = false;
    this.viewMode = true;
  }

  onEdit(presta: Prestation){
    this.editMode = true;
  }

  switchMode(){
    this.viewMode = false;
    this.editMode = false;
    this.createMode = true;
  }

}
