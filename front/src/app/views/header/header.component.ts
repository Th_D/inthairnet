import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  connected = false;
  modeAdmin = false;

  constructor( public authService:AuthService) {}
 
  ngOnInit() {
    this.authService.currentUser.subscribe((data) => {
      console.error("HEADER => ", data);
      if (!data){
        // console.error("NO DATA => NOT CONNECTED");
        // this.logout();
        this.connected = false;
        this.modeAdmin = false;
      } else{
          this.connected = true;
          if (data['id'] === -1){
            // console.error("HEADER => ADMIN");
            this.modeAdmin = true;
          } else {
            // console.error("HEADER => NORMAL");
            this.modeAdmin = false;
          }
      }
    })
  }

  logout(){
    console.log("logout")
    this.modeAdmin = false;
    this.connected = false;
    this.authService.logout();
    swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'You have been deconnected',
      showConfirmButton: false,
      timer: 1500
    })
  }

}
