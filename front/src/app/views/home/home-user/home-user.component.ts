import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.scss']
})
export class HomeUserComponent implements OnInit {

  isLoaded: boolean = true;

 
  categories: any = [
    {
      img: "../../../assets/img/barber-shop.svg",
      label: "Manage a Shop",
      destination: "/shop-manage"
    },
    {
      img: "../../../assets/img/barber.svg",
      label: "Add a Prestation",
      destination: "/prestation-manage"
    },
    {
      img: "../../../assets/img/calendar.png",
      label: "View my schedule",
      destination: "/booking-manage"
    }
  ];

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
