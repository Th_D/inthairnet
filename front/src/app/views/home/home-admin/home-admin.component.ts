import { Component, OnInit } from '@angular/core'
import { ShopService } from '../../../services/shop.service';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.scss']
})
export class HomeAdminComponent implements OnInit {

  constructor(public shopService: ShopService) { }
  cards = [];
 

  slides: any = [[]];
  chunk(arr, chunkSize) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  ngOnInit() {
    this.getShops();
    // this.slides = this.chunk(this.cards, 3);
  }

  getShops() {
    this.shopService.getShops().subscribe(shops => {
      // this.shops = shops;
     
      console.log(shops)
      let i=1;
      shops.forEach(element =>   
    {
        // i = i + 1;
        this.cards.push({
          id : element.id,
          title: element.name,
          description: 'Some quick example text to build on the card title and make up the bulk of the card content',
          buttonText: 'Détails',
          img: '../../../assets/img/salon'+i+'.jpg'
        })
      }
      );
      this.slides = this.chunk(this.cards, 3);
    });

  }

}

// import { Component, OnInit } from '@angular/core';
// import { first } from 'rxjs/operators';

// import { UserService } from 'src/app/services/user.service';
// import { User } from 'src/app/models/user';

// @Component({
//   selector: 'app-home',
//   templateUrl: './home.component.html',
//   styleUrls: ['./home.component.scss']
// })
// export class HomeComponent implements OnInit {

//     users: User[] = [];

//     constructor(private userService: UserService) {}

//     ngOnInit() {
//         this.userService.getAll().pipe(first()).subscribe(users => { 
//             console.log(users)
//             this.users = users; 
//         });
//     }
// }