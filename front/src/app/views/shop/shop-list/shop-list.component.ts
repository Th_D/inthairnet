import { Component, OnInit } from '@angular/core';
import { ShopService } from '../../../services/shop.service';
declare var jQuery: any;

function filter_tab($) {
  $(document).ready(function(){
    $("#demo").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#test tr").filter(function() {
         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
   });
  });
}

@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.scss']
})
export class ShopListComponent implements OnInit {

  shops;
  selectedShop;

  constructor(public shopService: ShopService) { }

  ngOnInit() {
    this.shops = this.getShops();
    filter_tab(jQuery);
  }

  public selectShop(shop){
    this.selectedShop = shop;
  }

  getShops() {
    this.shopService.getShops().subscribe(shops => {
      this.shops = shops;
      console.log(this.shops);
    });
  }

  public deleteShop(shop){
    console.log("Delete Shop ID = ",shop.id);
    this.shopService.deleteShop(shop.id).subscribe(shops => {
      console.log(shops);
      this.getShops();
    });
  }

}
