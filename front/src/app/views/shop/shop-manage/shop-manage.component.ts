import { Component, OnInit } from '@angular/core';
import { ShopService } from 'src/app/services/shop.service';
import { Shop } from 'src/app/models/shop';
import { Hairdresser } from 'src/app/models/hairdresser';
import { HairdresserService } from 'src/app/services/hairdresser.service';
import { AuthService } from 'src/app/services/auth.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shop-manage',
  templateUrl: './shop-manage.component.html',
  styleUrls: ['./shop-manage.component.scss']
})
export class ShopManageComponent implements OnInit {

  isLoaded: boolean = false;
  idShop: number;
  shop: Shop;
  hairdresser: Hairdresser;
  hairdressers:Hairdresser[];
  hairdressers_list:Hairdresser[];
  edition: boolean = false;
  selectedHairdId: number;
  selectedHaird: Hairdresser;

  constructor(private shopService: ShopService, private hairdresserService: HairdresserService, private authService: AuthService,private router: Router) {}

  ngOnInit() {
 
    const getCurrentUser= () => {
      return new Promise((resolve, reject) => {
        var currentId = this.authService.currentUser.source['_value']['id'];
        this.hairdresserService.getHaird(currentId).subscribe(data=> {
          this.selectedHaird = data;
          this.hairdresser = data;
          this.idShop = data.idShop;
          resolve(data);
        })       
      }); 
    };
  
    getCurrentUser()
        .then(step2 => {
          if (this.idShop)
            this.findOne(this.idShop);
        })
        .then(step3 => {
          if (this.idShop)
            this.findHairdressers(this.idShop);
          this.isLoaded = true;
        })
        .catch(error => console.error(error));    
  }


  findOne(id: number) {
    this.shopService.getShop(id).subscribe(shop => {
      this.shop = shop;
      // console.error("Our Shop ", this.shop)
      this.isLoaded = true;
    })
  }

  findHairdressers(id_shop:number){
    this.hairdresserService.getHairds().subscribe(hairdressers => {
      this.hairdressers_list = hairdressers;
      this.hairdressers = hairdressers.filter(e => {
        if(e.idShop == id_shop){
          return e;
        }
      })
      
      try{
        this.shop.hairdressers = this.hairdressers;
      } catch(error){
        console.error("Erreur, Initialisation du shop non terminé");
      }
    })
  }

  public addElement(ElementList, element) {
    let newList = Object.assign(ElementList, element)
    return newList
  }

  public selectHaird(hairdresser){
    // this.selectedHaird = hairdresser;
    // console.error(this.selectedHaird);
    // const control = <FormArray>this.shopForm.controls['hairdressers'];
    // control.push(this.initHaird(hairdresser.id, hairdresser.lastname, hairdresser.firstname));
    // var obj = {};
    // obj = this.addElement(this.shop.hairdressers,hairdresser);
    // this.shop.hairdressers = obj;
    console.log("TODO BUG INDICE")
    document.getElementById("closeModalButton").click();
    console.error(this.shop)
  }

  test(){
    this.router.navigate(['booking-details', this.selectedHairdId])
  }

  onHaird(hairdresser){
    // var removeIndex = this.shop.hairdressers.map(function(item) { return item.id; }).indexOf(37);
    // this.shop.removeAt(control.value.findIndex(image => image.id === team.value.id));
    // document.getElementById("closeModalButton").click();
   
    this.selectedHaird = hairdresser;
    this.selectedHairdId = hairdresser.id;
    console.log(this.selectedHairdId)
    // this.router.navigate(['booking-details', this.selectedHairdId])
    // document.getElementById("openModalButton").click();
  }

  onSave(){
    this.edition = false;
    console.error("MAJ => ",this.shop);
    this.shopService.updateShop(this.shop).subscribe(data => {
      swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Your shop has been updated successfully',
          showConfirmButton: false,
          timer: 1500
      })
    }, 
    err => {
      console.error("[LOG PUT]: ", err)
      swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Incorrect Data Format',
          text: 'Something went wrong!',
      })

    });
  }



}
