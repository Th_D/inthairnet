import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import swal from 'sweetalert2';
import { ShopService } from '../../../services/shop.service';
import { AuthService } from 'src/app/services/auth.service';
import { HairdresserService } from 'src/app/services/hairdresser.service';
import { Hairdresser } from 'src/app/models/hairdresser';

@Component({
  selector: 'app-shop-create',
  templateUrl: './shop-create.component.html',
  styleUrls: ['./shop-create.component.scss']
})

export class ShopCreateComponent implements OnInit {

  public shopForm : FormGroup;
  hairdressers_list: Hairdresser[];
  selectedHaird: Hairdresser;
  days: any = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];

  constructor(public shopService: ShopService, private router: Router, private _fb: FormBuilder, private authService : AuthService, private hairdresserService: HairdresserService) { }

  ngOnInit() {

    this.shopForm = this._fb.group({
      // name: new FormControl({value: 'Le nom', disabled: true}, Validators.required),
      name: "",
      address: "",
      city: "",
      openningHours: this._fb.array([]),
      hairdressers: this._fb.array([])
    });

    this.setOpenningHoursv2();
    this.findHairdressers();
  }


  openningHoursv2(currentDay): FormGroup {
    return this._fb.group({
      day: new FormControl(currentDay, Validators.required),
      hours: this._fb.array([this.hours])
    });
  }

  get hours(): FormGroup {
    return this._fb.group({
      opening: "09:00",
      ending: "18:00"
    });
  }

  get hairdressers(): FormGroup {
    return this._fb.group({
      id: "",
      lastname: "",
      firstname: "",
    });
  }


  setOpenningHoursv2(){
    // (this.shopForm.get("openningHours") as FormArray).push(this.openningHours);
    const control = <FormArray>this.shopForm.controls['openningHours'];
    for (let i = 0; i < this.days.length; i++) {
      console.log("Block statement execution no." + i + "  =>  " + this.days[i]);
      control.push(this.openningHoursv2(this.days[i]));
    }
  }

  initHaird(id, last, first) {
    return this._fb.group({
      id: id,
      lastname: last,
      firstname: first,
    });
  }


  initSkill(currentDay) {
    return this._fb.group({
      day: [currentDay, Validators.required],
      hours: this._fb.array([
        this.initHours(), 
      ])
    });
  }

  initHours() {
    return this._fb.group({
        opening: ['09:00', Validators.required],
        ending: ['18:00', Validators.required],
    });
  }
  
  setOpenningHours() {
      const control = <FormArray>this.shopForm.controls['openningHours'];
      for (let i = 0; i < this.days.length; i++) {
        console.log("Block statement execution no." + i + "  =>  " + this.days[i]);
        control.push(this.initSkill(this.days[i]));
      }
  }

  findHairdressers(){
    this.hairdresserService.getHairds().subscribe(hairdressers => {
      this.hairdressers_list = hairdressers;
    });
  }

  public selectHaird(hairdresser){
    this.selectedHaird = hairdresser;
    // console.error(this.selectedHaird);
    const control = <FormArray>this.shopForm.controls['hairdressers'];
    control.push(this.initHaird(hairdresser.id, hairdresser.lastname, hairdresser.firstname));
    document.getElementById("closeModalButton").click();
    console.error(this.shopForm.value)
  }

  onHaird(team){
    const control = <FormArray>this.shopForm.controls['hairdressers'];
    control.removeAt(control.value.findIndex(image => image.id === team.value.id));
  }

  createShop(){
    console.error(this.shopForm.value);
    this.shopService.createShop(this.shopForm.value).subscribe(data => {
      console.log("SUCESS ", data);
      if (data){
      swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your shop has been saved',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(() => {
        var currentId = this.authService.currentUser.source['_value']['id'];
        if (currentId == -1)
          this.router.navigate(['home-admin']);
        else
          this.router.navigate(['home-user']);
    }, 1750); 
    }else{
      swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Your shop hasn\'t been saved',
        text: 'Something went wrong!',
      })
    }


    });
  }

}
