import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Shop } from 'src/app/models/shop';
import { switchMap } from 'rxjs/operators';
import { ShopService } from 'src/app/services/shop.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Hairdresser } from 'src/app/models/hairdresser';
import { HairdresserService } from 'src/app/services/hairdresser.service';

@Component({
  selector: 'app-shop-details',
  templateUrl: './shop-details.component.html',
  styleUrls: ['./shop-details.component.scss']
})
export class ShopDetailsComponent implements OnInit {

  isLoaded: boolean = false;
  idShop: number;
  shop: Shop;
  hairdresser: Hairdresser;
  hairdressers:Hairdresser[];

  constructor(private route: ActivatedRoute, private router: Router, public shopService: ShopService, public hairdresserService: HairdresserService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.idShop = params['id'];
      console.log(this.idShop);

      this.shopService.getShop(this.idShop).subscribe(shop => {
        this.shop = shop;
        console.error("Our Shop ", this.shop)
        this.isLoaded = true;
      });

      this.hairdresserService.getHairds().subscribe(hairdressers => {
        this.hairdressers = hairdressers.filter(e => {
          if(e.idShop == this.idShop){
            return e;
          }
        })
      })

    })
  }

  back(){    
    this.router.navigate(['home']);
  }



}
