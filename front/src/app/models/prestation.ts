
export class Prestation {
  
    constructor(
      public id: number,
      public idHairdresser: number,
      public libelle: string,
      public price: number,
      public duration: number
    ) {  }
  
  }


