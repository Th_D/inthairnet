
export class Shop {
  
    constructor(
      public id: number,
      public name: string,
      public address: string,
      public city: string,
      public openningHours: object,
      public hairdressers: object
    ) {  }
  
  }
