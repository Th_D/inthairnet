import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopListComponent } from './views/shop/shop-list/shop-list.component';
import { ShopCreateComponent } from './views/shop/shop-create/shop-create.component';
import { ShopDetailsComponent } from './views/shop/shop-details/shop-details.component';
import { ShopManageComponent } from './views/shop/shop-manage/shop-manage.component';
import { HomeAdminComponent } from './views/home/home-admin/home-admin.component';
import { HomeUserComponent } from './views/home/home-user/home-user.component';
import { UserSigninComponent } from './views/login/user-signin/user-signin.component';
import { UserSignupComponent } from './views/login/user-signup/user-signup.component';
import { HairdresserCreateComponent } from './views/hairdresser/hairdresser-create/hairdresser-create.component';
import { HairdresserListComponent } from './views/hairdresser/hairdresser-list/hairdresser-list.component';
import { HairdresserDetailsComponent } from './views/hairdresser/hairdresser-details/hairdresser-details.component';
import { HairdresserManageComponent } from './views/hairdresser/hairdresser-manage/hairdresser-manage.component';
import { PrestationManageComponent } from './views/prestation/prestation-manage/prestation-manage.component';
import { BookingManageComponent } from './views/booking/booking-manage/booking-manage.component';
import { PrestationListComponent } from './views/prestation/prestation-list/prestation-list.component'; 
import { BookingListComponent } from './views/booking/booking-list/booking-list.component'; 
import { BookingDetailsComponent } from './views/booking/booking-details/booking-details.component'; 

import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

const routes: Routes = [
  {path: "user-signin", component: UserSigninComponent},
  {path: "user-signup", component: UserSignupComponent},  

  {path: "home-admin", component: HomeAdminComponent, canActivate: [AdminGuard]},
  {path: "home-user", component: HomeUserComponent, canActivate: [AuthGuard]},

  {path: "shop-list", component: ShopListComponent, canActivate: [AdminGuard]},
  {path: "shop-create", component: ShopCreateComponent, canActivate: [AuthGuard]},
  {path: "shop-manage", component: ShopManageComponent, canActivate: [AuthGuard]},
  {path: "shop-details/:id", component: ShopDetailsComponent, canActivate: [AuthGuard]},

  {path: "hairdresser-list", component: HairdresserListComponent, canActivate: [AdminGuard]},   
  {path: "hairdresser-create", component: HairdresserCreateComponent},  
  {path: "hairdresser-manage", component: HairdresserManageComponent, canActivate: [AuthGuard]},
  {path: "hairdresser-details/:id", component: HairdresserDetailsComponent, canActivate: [AuthGuard]},

  {path: "prestation-list", component: PrestationListComponent, canActivate: [AdminGuard]},
  {path: "prestation-manage", component: PrestationManageComponent, canActivate: [AuthGuard]},

  {path: "booking-list", component: BookingListComponent, canActivate: [AdminGuard]},
  {path: "booking-manage", component: BookingManageComponent, canActivate: [AuthGuard]},
  {path: "booking-details/:id", component: BookingDetailsComponent, canActivate: [AuthGuard]},
  
   // otherwise redirect to home
   { path: '**', redirectTo: 'user-signin' }        
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
