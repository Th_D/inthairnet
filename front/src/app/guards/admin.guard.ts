import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {

    constructor(private router: Router,  public authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var storage = localStorage.getItem('currentUser');
        try{
        var isAdmin = (this.authService.currentUser.source['_value']['id'] === -1 ? true : false);
        }catch(error){
            console.error("Not connected, Not allowed");
            this.router.navigate(['/user-login'], { queryParams: { returnUrl: state.url }});
        }

        if (storage && isAdmin) {
            // logged in mode admin so return true
            return true;
        } 

        // not logged in so redirect to login page with the return url
        if (storage){
            this.router.navigate(['/home-user'], { queryParams: { returnUrl: state.url }});
        } else{
            this.router.navigate(['/user-login'], { queryParams: { returnUrl: state.url }});
        }
        return false;
    }
}